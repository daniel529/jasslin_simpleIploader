﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UTC;

namespace Skyeye.Simulator
{
    public class TrackingObject
    {
        public string Vehicle_id { get; set; }
        public string Tracker_time { get; set; }
        public string Server_time { get; set; }
        public string Position_status { get; set; }
        public string Longitude { get; set; }
        public string Latidude { get; set; }
        public string Direction { get; set; }
        public string Speed { get; set; }
        public string Trip { get; set; }
        public string Rpm { get; set; }
        public string Sensor_temp { get; set; }
        public string Instant_fuel_level { get; set; }
        public string Average_fuel_level { get; set; }
        public bool Acc { get; set; }
        public bool Front_gate { get; set; }
        public bool Rear_gate { get; set; }
        public bool Sos { get; set; }
        public TrackingObject(string vehicle_id)
        {
            Acc = true;
            Front_gate = false;
            Rear_gate = true;
            Sos = false;
            Position_status = "A";
            Trip = "1000";
            Direction = "70";
            Speed = new Random().Next(6,99).ToString();
            Rpm = "1679";
            Average_fuel_level = "705";
            Sensor_temp = "029";
            Instant_fuel_level = "0000";
            Tracker_time = DateTime.UtcNow.ToString("ddMMyy");
            Server_time = DateTime.UtcNow.ToString("hhmmss");
            Vehicle_id = vehicle_id;
            Latidude = (new Random().NextDouble() * (21.038332 - 21.025386) + 21.025386).ParseToDMS("NS");
            Longitude = (new Random().NextDouble() * (105.841599 - 105.817909) + 105.817909).ParseToDMS("WE");
            //Latidude = "0";
            //Longitude = "0";
        }
        public override string ToString()
        {
            List<string> lstAttributes = new List<string>();
            lstAttributes.Add("$");
            Type t = this.GetType();
            foreach (PropertyInfo pi in t.GetProperties())
            {
                if (pi.Name.Equals("Acc") || pi.Name.Equals("Front_gate") || pi.Name.Equals("Rear_gate") || pi.Name.Equals("Sos"))
                {
                    if (bool.Parse(pi.GetValue(this, null) + ""))
                        lstAttributes.Add("1");
                    else
                        lstAttributes.Add("0");
                }
                else
                    lstAttributes.Add(pi.GetValue(this, null) + "");

            }
            //lstAttributes.Add("#");
            string last = string.Join(",", lstAttributes.ToArray());
            last = last + "#";
            return last;
        }
    }
}
