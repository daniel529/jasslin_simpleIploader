﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skyeye.Simulator
{
    public static class GeoAngle
    {
        public static string ParseToDMS(this double value, string type)
        {
            StringBuilder sb = new StringBuilder();
            if(type.Equals("NS"))
            {
                if (value <= 0)
                    sb.Append("S");
                else sb.Append("N");
            }
            else if(type.Equals("WE"))
            {
                if (value <= 0)
                    sb.Append("W");
                else sb.Append("E");
            }
            double min = (value - Math.Floor(value))*60;
            sb.Append(Math.Floor(value) + "");
            if (Math.Floor(min) >= 10)
                sb.Append(Math.Floor(min));
            else
                sb.Append(Math.Floor(min).ToString("00"));
            sb.Append("."+ ((min - Math.Floor(min))+"").Replace(".",String.Empty));
            return sb.ToString();
        }
    }
}
