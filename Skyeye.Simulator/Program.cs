﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Messaging;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Collections;

namespace Skyeye.Simulator
{
    class Program
    {
        static void Main(string[] args)
        {
            Client.init();
            new Client().Start();
            Console.ReadKey();
        }
    }
    public class Client
    {
        public static bool isContinue = true;
        public static DateTime startTime;
        public static int CURRENT_COUNT = 0;
        public static int TRACKING_COUNT = 0;
        private static string IP;
        private static int PORT;
        private static int MAX_DEVICE;
        private static string FILENAME;
        private static int DELAY_TIME;
        private static readonly Random random = new Random();
        private static readonly object syncLock = new object();
        private static readonly object checkLock = new object();
        private static readonly object countLock = new object();
        private static bool isStart = false;
        private static int count = 0;
        private static int trip = 0;
        private static String binaryText;
        private static List<String> packetList2 = new List<string>();
        public static void init()
        {
            IP = ConfigurationManager.AppSettings["IP"];
            PORT = Convert.ToInt32(ConfigurationManager.AppSettings["PORT"]);
            MAX_DEVICE = Convert.ToInt32(ConfigurationManager.AppSettings["MAXDEVICE"]);
            FILENAME = ConfigurationManager.AppSettings["FILENAME"];
            DELAY_TIME = Convert.ToInt32(ConfigurationManager.AppSettings["DELAYTIME"]);
            Console.WriteLine("Init Done");
            binaryText = File.ReadAllText("E:\\jasslin\\2020-11-27_RawData.bin");
            String[] packet = Regex.Split(binaryText, "\r\n");
            
            for (int i = 0; i < packet.Length; i++)
            {
                packetList2.Add(packet[i]);

            }
        }
        public void Start_02()
        {
            TcpClient tcp = new TcpClient(IP, PORT);
            Console.WriteLine("Client connected: " + tcp.Connected);
            Stream stm = tcp.GetStream();
            StreamWriter sw = new StreamWriter(stm);
            sw.AutoFlush = true;
            TrackingObject objLast = new TrackingObject("ENDING");
            sw.WriteLine(objLast.ToString());
        }
        public void Start()
        {
            startTime = DateTime.Now;
            for (int i = 1; i <= MAX_DEVICE; i++)
            {
                Thread thead = new Thread(() =>
                {
                    SpecialSending(i);
                });
                thead.IsBackground = true;
                thead.Start();
            }
            Console.WriteLine("Done");
        }
        public void SendingData_02(string vehicle_id)
        {
            if (isContinue)
            {
                TcpClient tcp = new TcpClient(IP, PORT);
                Console.WriteLine("Client connected: " + tcp.Connected + " " + count);
                lock (syncLock)
                {
                    count++;
                }
                Stream stm = tcp.GetStream();
                StreamWriter sw = new StreamWriter(stm);
                sw.AutoFlush = true;
                Thread.Sleep(DELAY_TIME);
                while (true)
                {
                    lock (checkLock)
                    {
                        TRACKING_COUNT++;
                        if (TRACKING_COUNT == 20000)
                        {
                            File.WriteAllText("../../result.txt", "Finish in: " + (DateTime.Now.Subtract(startTime).TotalSeconds + 30));
                            while (true)
                            {

                            }
                        }

                    }
                    TrackingObject objTracking = new TrackingObject(vehicle_id);
                    String s = objTracking.ToString();
                    Console.WriteLine(objTracking.ToString());
                    sw.WriteLine(objTracking.ToString());
                    Thread.Sleep(DELAY_TIME);

                }
            }
        }
        public void SpecialSending(int index) {
            try
            {
                IPAddress ipAddr = IPAddress.Parse("60.251.157.47");
                IPEndPoint ipEndPoint = new IPEndPoint(ipAddr,8106);
                Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                client.Connect(ipEndPoint);
                if (isContinue)
                {
                    foreach (var item in packetList2)
                    {
                        String value = item.Replace("KLD-2702", "danielCar-" + index)+"\r\n";
                        client.Send(Encoding.ASCII.GetBytes(value));
                        Console.WriteLine("Thread " + index + " sent");
                        Thread.Sleep(DELAY_TIME);
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
        public void SendingToMQ(string vehicle_id)
        {
            try
            {
                TrackingObject objTracking = new TrackingObject(vehicle_id);
                MessageQueue m_InsertQueue = new MessageQueue(@"FormatName:DIRECT=TCP:14.225.3.139\Private$\check_queue");
                m_InsertQueue.Formatter = new BinaryMessageFormatter();
                Message _myMessage = new Message(objTracking.ToString(), new BinaryMessageFormatter());
                m_InsertQueue.Send(_myMessage);
                Console.WriteLine("Done");
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                Thread.Sleep(DELAY_TIME);
            }
        }
        }
}

